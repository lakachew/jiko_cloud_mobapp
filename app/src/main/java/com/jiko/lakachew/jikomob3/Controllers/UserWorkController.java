package com.jiko.lakachew.jikomob3.Controllers;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.util.Log;

import com.jiko.lakachew.jikomob3.Models.UserWork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by lakachew on 20/07/2016.
 */
public class UserWorkController {

    public UserWorkController(){}

    @NonNull
    public Collection getUserWorkObject(String response) throws JSONException {
        Collection collection = null;

        JSONArray jsonArray = new JSONArray(response);
        JSONObject jsonObject;

        for(int i=0; i<jsonArray.length();i++)
        {
            jsonObject = jsonArray.getJSONObject(i);

            collection.add(getUserWork(jsonObject) );

        }

        return collection;
    }


    public static UserWork getUserWork(JSONObject jsonObject) throws JSONException
    {
        UserWork userWork = new UserWork();

        userWork.setId(             jsonObject.getInt("id")                 );
        userWork.setCustomerId(     jsonObject.getInt("work_customer_id")   );
        userWork.setAddress(        jsonObject.getString("address")         );
        userWork.setDescription(    jsonObject.getString("description")     );
        userWork.setFinished(       jsonObject.getInt("finished")           );
        userWork.setCreated_at(     jsonObject.getString("created_at")      );
        userWork.setUpdated_at(     jsonObject.getString("updated_at")      );
        userWork.setUserId(         jsonObject.getInt("user_id")            );
        userWork.setWorkId(         jsonObject.getInt("work_id")            );


        Log.d("UserWorkController ID", "json:" + jsonObject.getInt("id") + " userWork:" + userWork.getId());
        Log.d("UserWorkCon Cust_ID", "json:" + jsonObject.getInt("work_customer_id") + " userWork:" + userWork.getCustomerId());
        Log.d("UserWorkCon address", "json:" + jsonObject.getString("address") + " userWork:" + userWork.getAddress());
        Log.d("UserWorkCon descritn", "json:" + jsonObject.getString("description") + " userWork:" + userWork.getDescription());
        Log.d("UserWorkCon finished", "json:" + jsonObject.getInt("finished") + " userWork:" + userWork.getFinished());
        Log.d("UserWorkCon created", "json:" + jsonObject.getString("created_at") + " userWork:" + userWork.getCreated_at());
        Log.d("UserWorkCon updated", "json:" + jsonObject.getString("updated_at") + " userWork:" + userWork.getUpdated_at());
        Log.d("UserWorkCon userID", "json:" + jsonObject.getInt("user_id") + " userWork:" + userWork.getUserId());
        Log.d("UserWorkCon workID", "json:" + jsonObject.getInt("work_id") + " userWork:" + userWork.getWorkId());

        return userWork;
    }

    public String[] getAssignedWorks(String jwtToken) {

        String[] values = null;

        return values;

    }


    public boolean setWorks(String response) {
        boolean isSet = false;
        ArrayList<String> stringArray;

        Log.d("Response asJSON:", response.toString());

        if(response != null)
        {
            stringArray = new ArrayList<String>();
            try {
                String[] splittedResponse = response.split(",");
                JSONArray jsonArray = new JSONArray(response);


                Log.d("Response asJSON2:", response.toString());

                for(int i=0; i< splittedResponse.length; i++)
                {
                    Log.d("Response asJSON i:", splittedResponse[i].toString());
                }

                for(int i=0; i<jsonArray.length(); i++)
                {
                    //createWork(jsonArray.getString(i));
                }

            } catch (JSONException e) {
                Log.d("Error asJSON:", response.toString());
                e.printStackTrace();
            }

            isSet = true;
        }



        return isSet;
    }

    private void createWork(String workArrayasString)
    {
        ContentValues contentValues = new ContentValues();

        String[] WorkStrings = workArrayasString.split(",");


        for (String content: WorkStrings)
        {
            String[] work = content.split(":");
            contentValues.put(work[0], work[1]);
        }

        Log.d("Response asContentValu", contentValues.toString());
    }
}

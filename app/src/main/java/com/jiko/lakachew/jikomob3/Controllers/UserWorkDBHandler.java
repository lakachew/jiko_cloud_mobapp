package com.jiko.lakachew.jikomob3.Controllers;

/**
 * Author : Lakew, Lakachew
 * Email  : Lakachew@gmail.com
 **/




import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jiko.lakachew.jikomob3.Models.UserWork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserWorkDBHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Jiko_Oy_3";

    // Login table name
    private static final String TABLE_NAME = "user_work";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_RECIVED_ID = "recived_id";
    private static final String KEY_WORK_ID = "work_id";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_CUSTOMER_ID = "customer_id";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_FINISHED = "finished";
    private static final String KEY_CREATED_AT = "created_at";
    private static final String KEY_UPDATED_AT = "updated_at";


    public UserWorkDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_USERWORK_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_RECIVED_ID + " INTEGER, "
                + KEY_WORK_ID + " INTEGER, "
                + KEY_USER_ID + " INTEGER, "
                + KEY_CUSTOMER_ID + " INTEGER, "
                + KEY_ADDRESS + " TEXT, "
                + KEY_DESCRIPTION + " TEXT, "
                + KEY_FINISHED + " INTEGER, "
                + KEY_CREATED_AT + " TEXT, "
                + KEY_UPDATED_AT + " TEXT" + ");";

        db.execSQL(CREATE_USERWORK_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public long addUserWork(UserWork userWork)
    {
        long insertId = -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_RECIVED_ID, userWork.getId());
        values.put(KEY_WORK_ID, userWork.getWorkId());
        values.put(KEY_USER_ID, userWork.getUserId());
        values.put(KEY_CUSTOMER_ID, userWork.getCustomerId());
        values.put(KEY_ADDRESS, userWork.getAddress());
        values.put(KEY_DESCRIPTION, userWork.getDescription());
        values.put(KEY_FINISHED, userWork.getFinished());
        values.put(KEY_CREATED_AT, userWork.getCreated_at());
        values.put(KEY_UPDATED_AT, userWork.getUpdated_at());

        Log.d("filling 1 userWorkId ", "" + userWork.getId());

        // Inserting Row
        insertId = db.insert(TABLE_NAME, null, values);

        db.close(); // Closing database connection

        Log.d("filling  2 userWorkId ", "" + userWork.getId());

        return insertId;
    }


    /**
     * Getting user data from database
     * */
    public UserWork getUserWorkById(int id){
        HashMap<String,String> user = new HashMap<String,String>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        String countQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE ID, ";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();

        UserWork userWork = new UserWork();

        if(cursor.getCount() > 0){
            userWork.setId(cursor.getInt(1));
            userWork.setWorkId(cursor.getInt(2));
            userWork.setUserId(cursor.getInt(3));
            userWork.setCustomerId(cursor.getInt(4));
            userWork.setAddress(cursor.getString(5));
            userWork.setDescription(cursor.getString(6));
            userWork.setFinished(cursor.getInt(7));
            userWork.setCreated_at(cursor.getString(10));
            userWork.setUpdated_at(cursor.getString(11));

        }
        cursor.close();
        db.close();
        // return user work
        return userWork;
    }



    /**
     * Getting user ID from database
     * */
    public int getUserid()
    {
        Log.d("Error: ", "Server side error 2");
        String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Move to first row
        cursor.moveToFirst();

        int id = cursor.getInt(cursor.getColumnIndex(KEY_ID));

        cursor.close();
        db.close();

        return id;
    }

    /**
     * Getting token from database
     * *//*
    public String getToken()
    {
        Log.d("Error: ", "Server side error 2");
        String selectQuery = "SELECT " + KEY_TOKEN + " FROM " + TABLE_LOGIN ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Move to first row
        cursor.moveToFirst();

        String token = cursor.getString(cursor.getColumnIndex(KEY_TOKEN));

        cursor.close();
        db.close();

        return token;
    }*/

    /**
     * Getting number of user work's if available
     * */
    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        cursor.close();

        // return row count
        return rowCount;
    }




    /**
     * returns the userWork object as a List
     * @return List<UserWork>
     */
    public List<UserWork> getAllUserWorkAsList()
    {
        Log.d("getAllUserWorkAsList", "i am here 0");
        UserWork userWork;
        List<UserWork> userWorkList = new ArrayList<>();

        Log.d("getAllUserWorkAsList", "i am here 1");
        String query = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("getAllUserWorkAsList", "i am here 2");
        Cursor cursor = db.rawQuery(query, null);

        Log.d("getAllUserWorkAsList", "i am here 3");

        if(cursor.moveToFirst())
        {
            do {
                userWork = new UserWork();
                userWork.setId(             cursor.getInt(cursor.getColumnIndex(KEY_RECIVED_ID))     );
                userWork.setCustomerId(     cursor.getInt(cursor.getColumnIndex(KEY_CUSTOMER_ID))     );
                userWork.setAddress(        cursor.getString(cursor.getColumnIndex(KEY_ADDRESS))  );
                userWork.setDescription(    cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION))  );
                userWork.setFinished(       cursor.getInt(cursor.getColumnIndex(KEY_FINISHED))     );
                userWork.setCreated_at(     cursor.getString(cursor.getColumnIndex(KEY_CREATED_AT))  );
                userWork.setUpdated_at(     cursor.getString(cursor.getColumnIndex(KEY_UPDATED_AT))  );
                userWork.setUserId(         cursor.getInt(cursor.getColumnIndex(KEY_USER_ID))     );
                userWork.setWorkId(         cursor.getInt(cursor.getColumnIndex(KEY_WORK_ID))    );

                Log.d("UserWorkDBHandler", "id:" + cursor.getInt(cursor.getColumnIndex(KEY_RECIVED_ID))
                                            + "userWork Id:" + userWork.getId());
                Log.d("UserWorkDBHandler", "created at:" + cursor.getString(cursor.getColumnIndex(KEY_CREATED_AT))
                                            + "userWork createdAt:" + userWork.getCreated_at());
                Log.d("UserWorkDBHandler", "address:" + cursor.getString(cursor.getColumnIndex(KEY_ADDRESS))
                                            + "userWork address:" + userWork.getAddress());
                Log.d("UserWorkDBHandler", "description:" + cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION))
                        + "userWork Description:" + userWork.getDescription());
                userWorkList.add(userWork);

            } while (cursor.moveToNext());
        }
        db.close();

        return userWorkList;
    }

    /**
     * Re crate database
     * Delete all tables and create them again
     * */
    public void resetTables(){
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

}

package com.jiko.lakachew.jikomob3;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.jiko.lakachew.jikomob3.Controllers.MapController;
import com.jiko.lakachew.jikomob3.Controllers.UserWorkDBHandler;
import com.jiko.lakachew.jikomob3.Controllers.VollySingleton;
import com.jiko.lakachew.jikomob3.Models.User;
import com.jiko.lakachew.jikomob3.Models.UserWork;
import com.jiko.lakachew.jikomob3.Models.WorkLogMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /*
                Definitions for the View
     */
    public TextView workNumberTextView;
    public TextView addressTextView;
    public TextView descriptionTextView;
    public Button startButton;
    public Button stopButton;
    public Toolbar toolBar;

    /*
                Map definistions
     */
    public String startedMapResponse;
    public ProgressDialog pDialog;
    public boolean isStarted;
    public int startedWorkLogId;
    private WorkLogMap startedMap;

    /*
                PASSING CONTEX FOR THE INTENT
     */
    private Context mContext;

    /*
                parameters used for POST WORKLOG
     */
    private String jwtToken;
    private User user;
    private String CHECK_START_MAP_URL = "https://api-jiko.appcloud.jubic.net/api/v4/authenticate/user/worklog";
    private String POST_START_MAP_URL = "https://api-jiko.appcloud.jubic.net/api/v4/authenticate/user/worklog/start";
    private String POST_STOP_MAP_URL = "https://api-jiko.appcloud.jubic.net/api/v4/authenticate/user/worklog/stop";

    private UserWorkDBHandler dbHandler;

    /*
                Definition for accessing Location or Google Service
     */
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private GoogleApiClient mGoogleApiClient;
    protected Location location;
    public double latitude;
    public double longitude;

    /*
                Definitions required for manupilation
     */
    public boolean startWork;
    private String RESTART = "restartActivity";
    private String EXIT = "exitActivity";
    private UserWork userWork;



    @Override
    protected void onStop() {
        super.onStop();
        if(mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if(mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.work_activity);

        /**
         *  Initializing the views
         */
        workNumberTextView = (TextView) findViewById(R.id.work_id);
        addressTextView = (TextView) findViewById(R.id.address_content);
        descriptionTextView = (TextView) findViewById(R.id.description_content);
        startButton = (Button) findViewById(R.id.startButton);
        stopButton = (Button) findViewById(R.id.stopButton);
        toolBar = (Toolbar) findViewById(R.id.worklog_toolbar);

        startedMap = null;
        isStarted = false;
        startedWorkLogId = -1;

        /**
         *      Setting toolbar with back button and title
         */
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle(R.string.log_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*
                    Getting the user token
         */
        User user = User.getInstance();
        jwtToken = user.getToken();

        /*
                    get the UserWork using the attached position
        */
        int userWorkPosition = (int) getIntent().getSerializableExtra("user_work_list_id");
        UserWorkDBHandler dbHandler = new UserWorkDBHandler(this);
        List<UserWork> userWorkList = dbHandler.getAllUserWorkAsList();
        Log.d("userWorkPosition", "id: " + userWorkPosition);
        userWork = userWorkList.get(userWorkPosition);

        Log.d("userWork content ", "id" + userWork.getId());
        Log.d("userWork content ", "address:" + userWork.getAddress());
        Log.d("userWork content ", "description" + userWork.getDescription());

        //setting the discription with the work Info
        workNumberTextView.setText("WN-"+userWork.getId());
        addressTextView.setText(userWork.getAddress());
        descriptionTextView.setText(userWork.getDescription());

        mContext = this.getApplicationContext();

        /*
                    Set startedMap Response
         */
        pDialog = ProgressDialog.show(this, "Map status", "Checking ... ", true);
        checkStartedMap();
        pDialog.dismiss();

        /*
                    Adding button listener
         */
        startButton.setOnClickListener(buttonListener);
        stopButton.setOnClickListener(buttonListener);

        /*
                GOOGLE SERVICE FOR LOCATION
         */
        if(isGoogleServiceAvailable()){

            // start Google service
            buildGoogleService();

            Log.d("isGoogleServiceAvaila", "have started");


        }else{
            String errorMessage = "please turn on GPS and try again";
            //showErrorDialog(EXIT, errorMessage);
        }



    }


    /**
     *              Volley request for checking if the Map have started or not
     */
    private void checkStartedMap() {
        RequestQueue queue = VollySingleton.getmInstance(this.getApplicationContext()).getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, CHECK_START_MAP_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                startedMap = WorkLogMap.getInstance();

                Log.d("UserWork json","Response:" + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("Map jsonArray", "Response:" + jsonArray.toString());
                    JSONObject jsonObject;

                    jsonObject = jsonArray.getJSONObject(0);

                    Log.d("WorkLogMap ", "jsonObject Map" + jsonObject);

                    Log.d("jsonObject response ","id: "+ jsonObject.getInt("id"));

                    startedMap = MapController.getWorkLogMap(jsonObject);

                    isStarted = true;

                    Log.d("started Map ", "Id:" + startedMap.getId());

                }catch (JSONException e) {
                    e.printStackTrace();
                }

                /*
                    adjust the buttons if Map have started or not
                 */
                if(isStarted)
                {
                    Log.d("starting the Map", " isStarted has passed");
                    if(startedMap != null)
                    {
                        int start = startedMap.getStart();

                        Log.d("starting the Map", " Map value is not null and latitude=" + startedMap.getLatitude());
                        Log.d("starting the Map", " Map value is not null and map id=" + startedMap.getId());
                        Log.d("starting the Map", " Map value is not null and worklog Id=" + startedMap.getWorklogId());

                        if(start == 1)
                        {
                            Log.d("starting the Map", " start=" + start);

                            //deactivating the stopButton
                            startButton.setEnabled(false);
                            startButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primary_light));
                            stopButton.setEnabled(true);
                            stopButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.accent));

                            startWork = true;
                        }else {
                            Log.d("isStarted", "value is " + isStarted);

                            //deactivating the stopButton
                            startButton.setEnabled(true);
                            startButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.accent));
                            stopButton.setEnabled(false);
                            stopButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primary_light));
                        }
                    }
                }else {
                    Log.d("isStarted", "value is " + isStarted);

                    //deactivating the stopButton
                    startButton.setEnabled(true);
                    startButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.accent));
                    stopButton.setEnabled(false);
                    stopButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primary_light));
                }

                //pDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error != null)
                {
                    Log.d("Volley Error", "message: " + error.getMessage());
                }else {

                    Log.d("Volley Error", "response has Error but no message");
                }

                startButton.setEnabled(true);
                startButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.accent));
                stopButton.setEnabled(false);
                stopButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primary_light));

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "bearer{"+jwtToken+"}");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_work_id", ""+userWork.getId());
                return params;
            }
        };

        /*
                Limiting the retry request in 2 ways
         */
        /*int x=2;// retry count
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 48,
                x, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/

        /*stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    /*
        Method for Checking if the device support Google Service
     */
    private boolean isGoogleServiceAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS){
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else {
                Toast.makeText(this, "This device is not supported for using Jiko Oy application.", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }

        //Toast.makeText(this, "Google Service is Available.", Toast.LENGTH_LONG).show();
        return true;
    }

    /*
            Method for building the Google API
     */
    protected synchronized void buildGoogleService() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    /**
     * Get's and Set's the Latitude and Longitude from the location Services
     */
    private boolean isLocationFound() {

        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Toast.makeText(this, "Your location is: " + latitude + "/" + longitude, Toast.LENGTH_LONG).show();
            return true;
        }else {
            latitude = 63.0925796; // MANUAL SETTING
            longitude = 21.6516582; // MANUAL SETTING
            Toast.makeText(this, "Turn your GPS 'ON' for accurate location. Default location is assigned.",
                    Toast.LENGTH_LONG).show();
            return true; // MANUAL SETTING
        }

        //for restricting (LOCKING) the user, if the GPS is not ON.
        //return false;
    }

    /**
     *              Option item listener
     * @param item of type MenuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        finish();
        return super.onOptionsItemSelected(item);
    }

    /***
     *      Button Listener
     */
    private View.OnClickListener buttonListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {

            Button button = (Button) v;

            if(button.getText().equals("Start Work")){

                if(isLocationFound()){
                    /*
                                Starting to send the Data to the server
                      */
                    postStartMap();
                }
            }
            else if(button.getText().equals("End Work"))
            {
                if(isLocationFound()){
                    /*
                                Starting to send the Data to the server
                      */
                    postStopMap();
                }
            }
        }
    };

    /**
     *      Post the started Map
     */
    private void postStartMap() {
        RequestQueue queue = VollySingleton.getmInstance(this.getApplicationContext()).getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, POST_START_MAP_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                boolean isPosted = false;

                Log.d("Map Post Response", response.toString());

                startedMap = WorkLogMap.getInstance();

                Log.d("Post Map json","Response:" + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("post Map jsonArray", "Response:" + jsonArray.toString());
                    JSONObject jsonObject;

                    jsonObject = jsonArray.getJSONObject(0);

                    Log.d("Post map ", "jsonObject" + jsonObject);

                    Log.d("Posted map ","json object id: "+ jsonObject.getInt("id"));

                    startedMap = MapController.getWorkLogMap(jsonObject);

                    isPosted = true;
                    startedWorkLogId = startedMap.getWorklogId();

                    Log.d("Posted Map ", "Id:" + startedMap.getId());

                }catch (JSONException e) {
                    e.printStackTrace();
                }


                if(isPosted)
                {
                    startButton.setEnabled(false);
                    startButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primary_light));
                    stopButton.setEnabled(true);
                    stopButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.accent));
                    startWork = true;
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error != null)
                {
                    Log.d("Volly Error Response: ", error.getMessage());
                    String errorMessage = "Worklog can not be Uploaded/Downloaded";
                    //showErrorDialog(RESTART, errorMessage);
                }else {
                    Log.d("Volly Post Error: ", "Error response with no message");
                }

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "bearer{"+jwtToken+"}");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("latitude", ""+latitude);
                params.put("longitude", ""+longitude);
                params.put("description", ""+userWork.getDescription());
                params.put("user_work_id", ""+userWork.getId());
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    /**
     *      Post the stoped Map
     */
    private void postStopMap() {
        RequestQueue queue = VollySingleton.getmInstance(this.getApplicationContext()).getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, POST_STOP_MAP_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                boolean isPosted = false;

                Log.d("Map Stop Post Response", response.toString());

                startedMap = WorkLogMap.getInstance();

                Log.d("Post Stop Map json","Response:" + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("post Stop Map jsonArray", "Response:" + jsonArray.toString());
                    JSONObject jsonObject;

                    jsonObject = jsonArray.getJSONObject(0);

                    Log.d("Post Stop map ", "jsonObject" + jsonObject);

                    Log.d("Posted Stop map ","json object id: "+ jsonObject.getInt("id"));

                    startedMap = MapController.getWorkLogMap(jsonObject);

                    isPosted = true;

                    Log.d("Posted stop Map ", "Id:" + startedMap.getWorklogId());

                }catch (JSONException e) {
                    e.printStackTrace();
                }


                if(isPosted)
                {
                    startButton.setEnabled(true);
                    startButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.accent));
                    stopButton.setEnabled(false);
                    stopButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primary_light));
                    startWork = false;
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error != null)
                {
                    //Log.d("Volly Error Response: ", error.getMessage());
                    String errorMessage = "Worklog can not be Uploaded/Downloaded";
                    //showErrorDialog(RESTART, errorMessage);
                }else {
                    Log.d("Volly Post Error: ", "Error response with no message");
                }

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "bearer{"+jwtToken+"}");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                startedWorkLogId = startedMap.getWorklogId();
                params.put("latitude", ""+latitude);
                params.put("longitude", ""+longitude);
                params.put("worklog_id", ""+startedWorkLogId);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    /**
     *      Handle Volley Error
     */
    private AlertDialog.Builder builder;
    private void showErrorDialog(final String action, String message)
    {
        builder = new AlertDialog.Builder(getApplicationContext());
        builder.setTitle("Error connecting");
        builder.setMessage(message);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(action.equals(RESTART)){
                    onRestart();
                }else if (action.equals(EXIT)){
                    finish();
                }


            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}

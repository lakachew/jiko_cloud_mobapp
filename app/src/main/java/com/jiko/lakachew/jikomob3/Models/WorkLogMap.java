package com.jiko.lakachew.jikomob3.Models;

/**
 * Created by lakachew on 31/07/2016.
 */
public class WorkLogMap {

    /*
            "id":8,"worklog_id":23,"longitude":-58.877063,"latitude":-42.996099
            "start":1,"created_at":"2015-10-15 01:22:51","updated_at":"2018-04-12 23:33:15"}
     */

    private int id;
    private int worklogId;
    private Double longitude;
    private Double latitude;
    private int start;
    private String createdAt;
    private String updatedAt;

    private static WorkLogMap instance = new WorkLogMap();

    private WorkLogMap(){}

    public static WorkLogMap getInstance(){
        return instance;
    }

    public WorkLogMap getMap(){
        return this;
    }

    /**
     *      Setters and Getters
     */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorklogId() {
        return worklogId;
    }

    public void setWorklogId(int worklogId) {
        this.worklogId = worklogId;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}

package com.jiko.lakachew.jikomob3.Models;

/**
 * Created by lakachew on 16/07/2016.
 */
public class UserWork
{
    private int customerId;
    private int finished;
    private String address;
    private String description;
    private String createdAt;
    private String updatedAt;
    private int workId;
    private int id;
    private int userId;

    public UserWork(){}


    public int getCustomerId() {
        return customerId;
    }
    public void setCustomerId(int customer_id) { this.customerId = customer_id; }
    public int getUserId() { return userId; }
    public void setUserId(int userId) { this.userId = userId; }
    public int getWorkId() { return workId; }
    public void setWorkId(int workId) { this.workId = workId; }
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public int getFinished() {
        return finished;
    }
    public void setFinished(int finished) { this.finished = finished; }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getCreated_at() {return createdAt;}
    public void setCreated_at(String created_at) {
        this.createdAt = created_at;
    }
    public String getUpdated_at() { return updatedAt; }
    public void setUpdated_at(String updated_at) {
        this.updatedAt = updated_at;
    }

}

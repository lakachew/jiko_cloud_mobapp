package com.jiko.lakachew.jikomob3.Controllers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by lakachew on 19/07/2016.
 */
public class VollySingleton {

    private static VollySingleton mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;

    private VollySingleton(Context context)
    {
        mCtx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized VollySingleton getmInstance(Context context)
    {
        if(mInstance==null)
        {
            mInstance = new VollySingleton(context);
        }

        return mInstance;
    }

    public RequestQueue getRequestQueue()
    {
        if(requestQueue==null)
        {
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }

        return requestQueue;
    }
    public <T> void addToRequestQue(Request<T> request)
    {
        requestQueue.add(request);
    }
}

package com.jiko.lakachew.jikomob3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jiko.lakachew.jikomob3.Controllers.MyRecyclerAdapter;
import com.jiko.lakachew.jikomob3.Controllers.UserController;
import com.jiko.lakachew.jikomob3.Controllers.UserWorkController;
import com.jiko.lakachew.jikomob3.Controllers.UserWorkDBHandler;
import com.jiko.lakachew.jikomob3.Controllers.VollySingleton;
import com.jiko.lakachew.jikomob3.Models.User;
import com.jiko.lakachew.jikomob3.Models.UserWork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserWorkActivity extends AppCompatActivity
{
    private String jwtToken;
    private User user;
    private String WORKSURL = "https://api-jiko.appcloud.jubic.net/api/v4/authenticate/user/works";
    //private String WORKSURL = "http://88.113.33.199:8000/api/v4/authenticate/user/works";

    private UserWorkDBHandler dbHandler;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private MyRecyclerAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Toolbar toolbar;        // Declaring the Toolbar Object

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //dbHandler.resetTables();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //dbHandler.resetTables();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_work_list_layout);

        toolbar = (Toolbar) findViewById(R.id.works_tolbar); // Attaching the layout to the toolbar object

        setSupportActionBar(toolbar);


        ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout)).setTitle("Assigned Works");

        //Collecting the attached assigned userWork information
        /*if(((String) getIntent().getSerializableExtra("token").toString()).length() > 10)
        {
            this.jwtToken = (String) getIntent().getSerializableExtra("token");
        }else
        {
            this.user = User.getInstance();
            this.jwtToken = user.getToken();
        }*/


        this.user = User.getInstance();
        this.jwtToken = user.getToken();

        /*      Get the works from the api      */
        //this.user = this.getUser();


        /*      collect, store and check if setted in db         */
        dbHandler = new UserWorkDBHandler(this);
        setUserWork(jwtToken);

        /*      holding the user works as a List<Object>        */
        List<UserWork> userWorkList;
        userWorkList = dbHandler.getAllUserWorkAsList();

        /**
         *              Recycle View
         *                  and
         *           MyRecyclerAdapter
         */

        adapter = new MyRecyclerAdapter(this, userWorkList);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        //recyclerView.addOnItemTouchListener();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.userwork_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            default:
                finish();
                startActivity(getIntent());
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     *     Get the user Object from the token
     * @return
     */
    private User getUser()
    {
        UserController userController = UserController.getInstance();
        userController.setUser(this.jwtToken);
        user = userController.getUserObject();
        return user;
    }

    /**
     * Connects to the server using volley and stores it to the SQLite
     * @param jwtToken
     */
    protected void setUserWork(final String jwtToken)
    {
        RequestQueue queue = VollySingleton.getmInstance(this.getApplicationContext()).getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, WORKSURL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                UserWork userWork;

                Log.d("UserWork json","Response:" + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("UserWork jsonArray", "Response:" + jsonArray.toString());
                    JSONObject jsonObject;
                    long insertId = 0;

                    dbHandler.resetTables();

                    for(int i=0; i<jsonArray.length();i++)
                    {
                        jsonObject = jsonArray.getJSONObject(i);

                        Log.d("UserWork ", "single jsonObject" + jsonObject);

                        Log.d("jsonObject response ","id: "+ jsonObject.getInt("id"));

                        userWork = UserWorkController.getUserWork(jsonObject);

                        insertId = dbHandler.addUserWork(userWork);

                        if(insertId == -1)
                        {
                            //showErrorDialog();
                            finish();
                        }

                        Log.d("user work isSet", "insertedId = " + insertId);


                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //showErrorDialog();
                //Log.d("Volly Error Response: ", error.getMessage());
                finish();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "bearer{"+jwtToken+"}");
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    /**
     *      Handle Volley Error
     */
    private AlertDialog.Builder builder;
    private void showErrorDialog()
    {
        builder = new AlertDialog.Builder(getApplicationContext());
        builder.setTitle("Error connecting");
        builder.setMessage("Works can not be collected from server");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                finish();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


}


package com.jiko.lakachew.jikomob3.Controllers;

import android.util.Log;

import com.jiko.lakachew.jikomob3.Models.User;

import java.util.regex.Pattern;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

/**
 * Created by lakachew on 17/07/2016.
 */
public class UserController
{
    private static User user;
    private String token;

    private static UserController instance = new UserController();

    public static UserController getInstance(){
        return instance;
    }

    // constructor
    private UserController(){
        this.user = User.getInstance();
    }

    /**
     * @return User object
     */
    public User getUserObject()
    {
        return user;
    }

    /**
     * Generates the user object content from the authorized jwt-auth token
     * returns true if authenticated
     * @return boolean
     */
    public boolean setUser(String token)
    {
        this.token = token;
        boolean isSet = false;

        String headerString = "";
        String payloadString = "";
        String jwtHeaderAndClaim = "";

        if (this.token.contains("."))
        {
            String[] jwtArray = this.token.split(Pattern.quote("."));
            headerString = jwtArray[0];
            payloadString = jwtArray[1];
            jwtHeaderAndClaim = headerString + "." + payloadString + ".";
        }
        else
        {
            throw new IllegalArgumentException("jwtString: " + this.token + "does not contain the character . ");
        }

        try {
            Claims claims = Jwts.parser().parseClaimsJwt(jwtHeaderAndClaim).getBody();

            int id = Integer.parseInt(claims.getSubject().toString());
            user.setId(id);
            user.setEmail(claims.get("email").toString());
            user.setFirstName(claims.get("first_name").toString());
            user.setLastName(claims.get("last_name").toString());
            user.setPrivilege(claims.get("privilege").toString());
            user.setCreateAt(claims.get("created_at").toString());
            user.setToken(this.token);

            isSet = true;

        }catch (Exception e){
            Log.e("Claims Exception ", e.toString());
        }

        return isSet;
    }
}

package com.jiko.lakachew.jikomob3.Controllers;

import android.util.Log;

import com.jiko.lakachew.jikomob3.Models.UserWork;
import com.jiko.lakachew.jikomob3.Models.WorkLogMap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lakachew on 31/07/2016.
 */
public class MapController {

    public static WorkLogMap getWorkLogMap(JSONObject jsonObject) throws JSONException
    {
        WorkLogMap workLogMap = WorkLogMap.getInstance();


    /*
            "id":8,"worklog_id":23,"longitude":-58.877063,"latitude":-42.996099
            "start":1,"created_at":"2015-10-15 01:22:51","updated_at":"2018-04-12 23:33:15"}
     */

        workLogMap.setId(             jsonObject.getInt("id")                 );
        workLogMap.setWorklogId(     jsonObject.getInt("worklog_id")   );
        workLogMap.setLongitude(        jsonObject.getDouble("longitude")         );
        workLogMap.setLatitude(    jsonObject.getDouble("latitude")     );
        workLogMap.setStart(       jsonObject.getInt("start")           );
        workLogMap.setCreatedAt(     jsonObject.getString("created_at")      );
        workLogMap.setUpdatedAt(     jsonObject.getString("updated_at")      );


        Log.d("MapController ID", "json:" + jsonObject.getInt("id") + " Map:" + workLogMap.getId());
        Log.d("MapController worklogId", "json:" + jsonObject.getInt("worklog_id") + " workLogMap:" + workLogMap.getWorklogId());
        Log.d("MapController longitude", "json:" + jsonObject.getDouble("longitude") + " workLogMap:" + workLogMap.getLongitude());
        Log.d("MapController latitude", "json:" + jsonObject.getDouble("latitude") + " workLogMap:" + workLogMap.getLatitude());
        Log.d("MapController start", "json:" + jsonObject.getInt("start") + " workLogMap:" + workLogMap.getStart());
        Log.d("MapController created", "json:" + jsonObject.getString("created_at") + " workLogMap:" + workLogMap.getCreatedAt());
        Log.d("MapController updated", "json:" + jsonObject.getString("updated_at") + " workLogMap:" + workLogMap.getUpdatedAt());

        return workLogMap;
    }
}

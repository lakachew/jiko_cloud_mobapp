package com.jiko.lakachew.jikomob3.Controllers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jiko.lakachew.jikomob3.R;

/**
 * Created by lakachew on 26/07/2016.
 */
public class CustomViewHolder extends RecyclerView.ViewHolder {
    protected TextView titleTextView, contentTextView;
    protected LinearLayout rowLayout;

    public CustomViewHolder(View itemView)
    {
        super(itemView);

        this.rowLayout = (LinearLayout) itemView.findViewById(R.id.clickable_row);
        this.titleTextView = (TextView) itemView.findViewById(R.id.title);
        this.contentTextView = (TextView) itemView.findViewById(R.id.content);
    }

}

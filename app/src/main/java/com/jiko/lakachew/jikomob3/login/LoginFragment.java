package com.jiko.lakachew.jikomob3.login;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jiko.lakachew.jikomob3.Controllers.UserController;
import com.jiko.lakachew.jikomob3.Controllers.VollySingleton;
import com.jiko.lakachew.jikomob3.R;
import com.jiko.lakachew.jikomob3.UserWorkActivity;

import java.util.HashMap;
import java.util.Map;


public class LoginFragment extends Fragment {

    // Vuew definitions
    private View view;
    private Button signInButton;
    private TextView forgetPassword;
    private EditText inputEmail;
    private EditText inputPassword;

    private AlertDialog.Builder builder;

    private static String LOGINURL = "https://api-jiko.appcloud.jubic.net/api/v4/authenticate";
    //private static String LOGINURL = "http://88.113.33.199:8000/api/v4/authenticate";

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.login_fragment, container, false);

        //Initializing the view contents
        inputEmail = (EditText) view.findViewById(R.id.email);
        inputPassword = (EditText) view.findViewById(R.id.password);
        signInButton = (Button) view.findViewById(R.id.signInButton);
        forgetPassword = (TextView) view.findViewById(R.id.forgPaWord);

        /*
        // On Click Listener for the forgetPassword Text Button
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Defining, initializing and forwarding to the Password Reset Activity
                Intent myIntent = new Intent(view.getContext(), PasswordResetActivity.class );
                startActivityForResult(myIntent,0);
            }
        });*/

        /**
         * Login button Click Listener
         **/
        signInButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                // if Statement: for checking if the input values have been properly inserted and show requirment if needed
                if ((!inputEmail.getText().toString().equals("")) && (!inputPassword.getText().toString().equals("")))
                {
                    //NetAsync(view);
                    // calling the volley library for web authentication
                    login(inputEmail.getText().toString(), inputPassword.getText().toString(), getActivity());

                } else if ((!inputEmail.getText().toString().equals(""))) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Password is required", Toast.LENGTH_SHORT).show();
                } else if ((!inputPassword.getText().toString().equals(""))) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Email is required", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Email and Password are required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }


    protected void login(String email, String password, final FragmentActivity activity)
    {
        final String emailString = email;
        final String passwordString = password;
        Log.d("Login Volly: ", "is about to start ...");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGINURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                Intent userPanel = new Intent(getActivity().getApplicationContext(), UserWorkActivity.class);
                //userPanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                UserController userController = UserController.getInstance();

                response = response.substring(1,response.length()-1);

                Log.d("Login Response", response.toString());

                if(userController.setUser(response))
                {
                    userPanel.putExtra("token", response);
                    startActivity(userPanel);
                    onStart();
                }else {
                    showErrorDialog();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(error != null){
                    showErrorDialog();
                    onStart();
                    Log.d("Volly Error Response: ", error.getMessage());
                }
                onStart();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", emailString);
                params.put("password", passwordString);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        //mRequestQueue.add(stringRequest);
        VollySingleton.getmInstance(getContext().getApplicationContext()).addToRequestQue(stringRequest);

    }

    private void showErrorDialog()
    {
        builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Sign in Error");
        builder.setMessage("Wrong user name or password");

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                inputPassword.setText("");
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        onStart();
    }

}

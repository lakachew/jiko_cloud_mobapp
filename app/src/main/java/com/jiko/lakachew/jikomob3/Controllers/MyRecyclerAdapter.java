package com.jiko.lakachew.jikomob3.Controllers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jiko.lakachew.jikomob3.WorkActivity;
import com.jiko.lakachew.jikomob3.Models.UserWork;
import com.jiko.lakachew.jikomob3.R;

import java.util.List;

/**
 * Created by lakachew on 26/07/2016.
 */
public class MyRecyclerAdapter extends RecyclerView.Adapter<CustomViewHolder>
{
    protected Context mContext;
    private List<UserWork> userWorkList;
    private View mView;

    public MyRecyclerAdapter(Context context, List<UserWork> userWorkList)
    {
        this.mContext = context;
        this.userWorkList = userWorkList;

    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_work_layout,
                viewGroup, false);

        return new CustomViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, final int position)
    {
        UserWork userWork = userWorkList.get(position);
        /*      Settes the TextView         */
        customViewHolder.titleTextView.setText("WN-" + userWork.getId());
        customViewHolder.contentTextView.setText(userWork.getAddress());
        /*      Settes the ClickListener        */
        View.OnClickListener clickListener = getOnClickListener(position);
        customViewHolder.rowLayout.setOnClickListener(clickListener);
    }

    @NonNull
    public View.OnClickListener getOnClickListener(final int position) {
        return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserWork userWork = userWorkList.get(position);
                    Toast.makeText(mContext, userWork.getAddress(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, WorkActivity.class);
                    intent.putExtra("user_work_list_id", position);
                    mContext.startActivity(intent);
                }

            };
    }

    @Override
    public int getItemCount() {
        return (null != userWorkList ? userWorkList.size() : 0);
    }

}

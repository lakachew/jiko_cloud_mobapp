package com.jiko.lakachew.jikomob3.Models;

/**
 * Created by lakachew on 31/07/2016.
 */
public class WorkLog {

    /*
            "id":8,"worklog_id":23,"longitude":-58.877063,"latitude":-42.996099
            "start":1,"created_at":"2015-10-15 01:22:51","updated_at":"2018-04-12 23:33:15"}
     */

    private int id;
    private int worklogId;
    private Double longitude;
    private Double latitude;
    private int start;
    private String createdAt;
    private String updatedAt;


}
